class Parking
  attr_reader :slots

  def initialize(size)
    @slots = Array.new(size) { |i| nil }
  end

  def assign(registration, colour)
    @slots.each_with_index do |slot, index|
      if slot.nil?
        @slots[index] = {registration: registration, colour: colour}
        return "Allocated slot number: #{ index + 1 }"
      end
    end
    return "Sorry, parking lot is full"
  end

  def leave(slot)
    if !@slots.empty? && @slots.size >= slot && !@slots[slot-1].nil?
      @slots[slot - 1] = nil
      return "Slot number #{slot} is free"
    end
    return "Not found"
  end

  def find_by_color(color, key)
    if key == "slot"
      result = @slots.each_with_index.map{|slot, i| i + 1 if !slot.nil? && slot[:colour].eql?(color)}.compact
    elsif key == "registration"
      result = @slots.map{|slot| slot[:registration] if !slot.nil? && slot[:colour].eql?(color)}.compact
    end
    return "Not found" if result.empty?
    return result.join(', ')
  end
  
  def find_by_registration(registration)
    result = @slots.each_with_index.map{|slot, i| i + 1 if !slot.nil? && slot[:registration].eql?(registration)}.compact
    return "Not found" if result.empty?
    return result.join(', ')
  end
  
  def status
    result = "Slot No. Registration No Colour\n\n"
    @slots.each_with_index do |slot, index|
      unless slot.nil?
        result << "#{index + 1} #{slot[:registration]} #{slot[:colour]}\n\n"
      end
    end
    return result
  end
end

trap "SIGINT" do
  puts "Exiting"
  exit 130
end
$<.each do |line|
  next if line.nil? || line.empty? || line == "\n"
  
  vars = line.nil? ? [] : line.split
  command = vars[0]
  # print "Input: "
  # print "#{line}\n"
  
  if command == "create_parking_lot"
    size = vars[1].to_i
    @parking = Parking.new(size)
    result = "Created a parking lot with #{size} slots"
  # elsif @parking.nil?
  #   result = "Invalid Command. Input Parking Lot Size"
  elsif command == "park"
    result = @parking.assign(vars[1], vars[2])
  elsif command == "leave"
    result = @parking.leave(vars[1].to_i)
  elsif command == "registration_numbers_for_cars_with_colour"
    result = @parking.find_by_color(vars[1], "registration")
  elsif command == "slot_numbers_for_cars_with_colour"
    result = @parking.find_by_color(vars[1], "slot")
  elsif command == "slot_number_for_registration_number"
    result = @parking.find_by_registration(vars[1])
  elsif command == "status"
    result = @parking.status
  end

  # print "Output: "
  print "#{result} \n\n"
end
