require "./lib/parking.rb"
require "test/unit"

class TestParking < Test::Unit::TestCase
  def setup
    @registration = "KA-01-HH-7777"
    @colour = "Red"

    @non_zero_size = rand(1..15)

    @parking_zero = Parking.new(0)
    @parking_non_zero = Parking.new(@non_zero_size)
    @parking_full = Parking.new(@non_zero_size)

    (@non_zero_size).times do
      @parking_full.assign(@registration, @colour)
    end

    @parking_free_3_6 = Parking.new(6)
    6.times do
      @parking_free_3_6.assign(@registration, @colour)
    end
    @parking_free_3_6.leave(3)
    @parking_free_3_6.leave(6)

    @parking_colorful = Parking.new(5)
    @parking_colorful.assign("1", "Red")
    @parking_colorful.assign("2", "White")
    @parking_colorful.assign("3", "Red")
    @parking_colorful.assign("4", "Blue")
    @parking_colorful.assign("5", "Red")
  end

  test "must create according to size" do
    size = rand(1..15)
    parking = Parking.new(size)
    assert_equal(parking.slots.size, size)  
  end

  test "must create 0 size" do
    assert_equal(@parking_zero.slots.size, 0)  
  end


  test "must not assign when size is 0" do
    result = @parking_zero.assign(@registration, @colour)
    assert_equal(result, "Sorry, parking lot is full")
  end

  test "must not assign when full" do
    result = @parking_full.assign(@registration, @colour)
    assert_equal(result, "Sorry, parking lot is full")
  end

  test "must assign to closest" do
    result = @parking_free_3_6.assign(@registration, @colour)
    assert_equal(result, "Allocated slot number: 3")
  end

  test "must vacate slot" do
    result = @parking_free_3_6.leave(4)
    assert_nil(@parking_free_3_6.slots[3])
  end

  test "must not vacate when size is 0" do
    result = @parking_zero.leave(rand(1..15))
    assert_equal(result, "Not found")
  end

  test "must not vacate when size < slot" do
    result = @parking_non_zero.leave(@parking_non_zero.slots.size + 1)
    assert_equal(result, "Not found")
  end

  test "must not vacate when slot is already free" do
    result = @parking_free_3_6.leave(3)
    assert_equal(result, "Not found")
  end

  test "test find slot by color" do
    result = @parking_colorful.find_by_color("Red", "slot")
    assert_equal("1, 3, 5", result)
  end

  test "test find slot by color single result" do
    result = @parking_colorful.find_by_color("White", "slot")
    assert_equal("2", result)
  end

  test "must return not found when color not present in lot" do
    result = @parking_colorful.find_by_color("Green", "slot")
    assert_equal(result, "Not found")
  end

  test "test find registration by color" do
    result = @parking_colorful.find_by_color("Red", "registration")
    assert_equal("1, 3, 5", result)
  end

  test "must return not found when color not present in lot (registration)" do
    result = @parking_colorful.find_by_color("Green", "registration")
    assert_equal(result, "Not found")
  end

  test "test find by registration" do
    slot = rand(1..5)
    result = @parking_colorful.find_by_registration(slot.to_s)
    assert_equal(result, slot.to_s)
  end

  test "must return not found when registration is not in the lot" do
    result = @parking_colorful.find_by_registration("KA-01-HH-1234")
    assert_equal(result, "Not found")
  end
end